<hr>
<div class="container">
    <p class="text-muted">
        &copy; {{ date('Y') }} by Kamil Musiał.<br/>
    </p>
</div>

{{ HTML::script('bootstrap/js/bootstrap.min.js'); }}
{{ HTML::script('js/jquery-2.1.1.min.js'); }}