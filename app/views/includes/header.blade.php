<div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="/">Nexway</a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <div class="pull-right">
            @if( Auth::check() )
                <ul class="nav navbar-nav">
                    <li><a href="/logout">logout ({{ Auth::user()->username }})</a></li>
                </ul>
            @endif
        </div>
    </div>
</div>