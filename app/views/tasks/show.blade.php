@extends('layouts.main')
@section('content')
    <div class="row">
        <div class="col-md-6">
            {{ HTML::linkAction('TaskController@index', 'back', [], ['class' => 'btn btn-block btn-info']) }}
        </div>
        <div class="col-md-6">
            {{ HTML::linkAction('TaskController@edit', 'edit task', [$task->id], ['class' => 'btn btn-block btn-info']) }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-12"><h2>{{ $task->title }}</h2></div>
    </div>
    <div class="row">
        <div class="col-md-3">date: <b>{{ date("d/m/Y", $task->date) }}</b></div>
        <div class="col-md-5">assigned to: 
        <ul>
            @foreach ($task->users as $user)
                <li><b>{{ $user->username }}</b></li>
            @endforeach
        </ul>
        </div>
        <div class="col-md-4">status: <b>{{ $task->stat->status }}</b></div>
    </div>
    <div class="row">
        <div class="col-md-12"><blockquote>{{ $task->description }}</blockquote></div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h3>comments (<span class="text-success">{{ $comments->getTotal() }})</span></h3>
            @foreach ($comments as $comment)
                <blockquote>
                    <p>{{ $comment->comment }}</p>
                    <footer>{{ $comment->users->username }} <cite>{{ date("/d/m/Y H:i", $comment->timestamp) }}</cite></footer>
                </blockquote>
            @endforeach
        </div>
    </div>
    <div class="row text-center">
        {{ $comments->links() }}
    </div>

    @if($isAssigned)
        <div class="row">
            <div class="col-md-12">
                <form role="form" method="post">
                    <div class="form-group">
                        {{ Form::textarea('comment', null, array('placeholder' => 'comment', 'class' => 'form-control')) }}
                    </div>
                    {{ Form::token(); }}
                    {{ Form::button('comment', array('class' => 'btn btn-primary', 'type' => 'submit')) }}
                </form>
            </div>
        </div>
    @endif
@stop