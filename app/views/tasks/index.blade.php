@extends('layouts.main')
@section('content')
    {{ HTML::linkAction('TaskController@create', 'add new task', [], ['class' => 'btn btn-block btn-info']) }}
    @if( ! empty($tasks))
        <table class="table">
            <tr><th>title</th><th>date</th><th>status</th><th>assigned</th></tr>
            @foreach ($tasks as $task)
                <tr>
                    <td>
                        {{ HTML::linkAction('TaskController@show', $task->title, [$task->id]) }}</td>
                    <td>{{ date("d/m/Y", $task->timestamp) }}</td>
                    <td>{{ $task->stat->status }}</td>
                    <td>{{ $task->assigned }} {{($task->assigned == 1) ? 'user' : 'users' }}</td>
                </tr>
            @endforeach
        </table>
        <div class="row text-center">
            {{ $tasks->links() }}
        </div>
    @endif
@stop