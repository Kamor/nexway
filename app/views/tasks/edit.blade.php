@extends('layouts.main')
@section('content')
    <form role="form" method="post">
        <div class="form-group">
            {{ Form::label('text', 'title') }}
            {{ Form::text('title', $task->title, array('placeholder' => 'title', 'class' => 'form-control')) }}
        </div>
        <div class="form-group">
            {{ Form::label('text', 'description') }}
            {{ Form::textarea('desc', $task->description, array('placeholder' => 'description', 'class' => 'form-control')) }}
        </div>
        <div class="form-group">
            {{ Form::label('text', 'user') }}
            {{ Form::select('user[]', $users, $selectUsers, array('class' => 'form-control', 'multiple')); }}
        </div>
        @if ( $isAssigned )
            <div class="form-group">
                {{ Form::label('text', 'status') }}
                {{ Form::select('status', $statuses, $task->status_id, array('class' => 'form-control')); }}
            </div>
        @endif
        {{ Form::token(); }}
        {{ Form::button('save', array('class' => 'btn btn-primary', 'type' => 'submit')) }}
        {{ HTML::linkAction('TaskController@show', 'cancel', [$task->id], ['class' => 'btn btn-default']) }}
    </form>
@stop