@extends('layouts.main')
@section('content')
    <form role="form" method="post">
        <div class="form-group">
            {{ Form::label('text', 'title') }}
            {{ Form::text('title', Input::old('title'), array('placeholder' => 'title', 'class' => 'form-control')) }}
        </div>
        <div class="form-group">
            {{ Form::label('text', 'description') }}
            {{ Form::textarea('desc', Input::old('desc'), array('placeholder' => 'description', 'class' => 'form-control')) }}
        </div>
        <div class="form-group">
            {{ Form::label('text', 'user') }}
            {{ Form::select('user[]', $users, null, array('class' => 'form-control', 'multiple')); }}
        </div>
        <div class="form-group">
            {{ Form::label('text', 'status') }}
            {{ Form::select('status', $statuses, null, array('class' => 'form-control')); }}
        </div>
        {{ Form::token(); }}
        {{ Form::button('create', array('class' => 'btn btn-primary', 'type' => 'submit')) }}
        {{ HTML::linkAction('TaskController@index', 'cancel', [], ['class' => 'btn btn-default']) }}
    </form>
@stop