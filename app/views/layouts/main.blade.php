<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    @include('includes.head')
</head>

<body>
    <div class="container">
        <nav class="navbar navbar-default" role="navigation">
            @include('includes.header')
        </nav>
        @if (Session::has('message'))
            <div class="alert alert-{{ Session::get('messageType') }}">{{ Session::get('message') }}</div>
        @endif
        @if ($errors->has())
            <div class="alert alert-danger" role="alert">
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        @endif
        @yield('content')

        <footer class="footer">
            @include('includes.footer')
        </footer>
    </div>
</body>
</html>
