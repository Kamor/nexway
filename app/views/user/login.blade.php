@extends('layouts.main')
@section('content')
    {{ Form::open(array('url' => 'login')) }}
        <h1>Login</h1>

        <p>
            {{ $errors->first('username') }}
            {{ $errors->first('password') }}
        </p>

        <p>
            {{ Form::label('text', 'username') }}
            {{ Form::text('username', Input::old('username'), array('placeholder' => 'username', 'class' => 'form-control')) }}
        </p>

        <p>
            {{ Form::label('password', 'password') }}
            {{ Form::password('password', array('placeholder' => 'pass', 'class' => 'form-control')) }}
        </p>

        <p>{{ Form::submit('submit', array('placeholder' => 'pass', 'class' => 'btn btn-primary')) }}
        {{ HTML::linkAction('UserRegisterController@signup', 'create account', [], ['class' => 'btn btn-default']) }}</p>
    {{ Form::close() }}
@stop