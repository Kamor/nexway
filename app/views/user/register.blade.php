@extends('layouts.main')
@section('content')
    {{ Form::open(array('url' => 'signup')) }}
        <h1>Register</h1>

        <p>
            {{ Form::label('text', 'username') }}
            {{ Form::text('username', Input::old('username'), array('placeholder' => 'username', 'class' => 'form-control')) }}
        </p>

        <p>
            {{ Form::label('password', 'password') }}
            {{ Form::password('password', array('placeholder' => 'pass', 'class' => 'form-control')) }}
        </p>

        <p>
            {{ Form::label('confirmPassword', 'confirm password') }}
            {{ Form::password('confirmPassword', array('placeholder' => 'pass', 'class' => 'form-control')) }}
        </p>

        <p>
            {{ Form::label('email', 'email') }}
            {{ Form::text('email', Input::old('email'), array('placeholder' => 'email', 'class' => 'form-control')) }}
        </p>

        <p>{{ Form::submit('create account', array('placeholder' => 'pass', 'class' => 'btn btn-primary')) }}</p>
    {{ Form::close() }}
@stop