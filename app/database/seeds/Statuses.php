<?php
/**
 * Created by PhpStorm.
 * User: Kamor
 * Date: 2014-11-27
 * Time: 20:58
 */

class Statuses  extends Seeder
{

    public function run()
    {
        DB::table('statuses')->delete();
        Status::create(array(
            'status' => 'new',
            'shortname' => 'new',
        ));
        Status::create(array(
            'status' => 'in progress',
            'shortname' => 'inprogress',
        ));
        Status::create(array(
            'status' => 'to review',
            'shortname' => 'toreview',
        ));
        Status::create(array(
            'status' => 'closed',
            'shortname' => 'closed',
        ));
    }
} 