<?php
/**
 * Created by PhpStorm.
 * User: Kamor
 * Date: 2014-11-27
 * Time: 20:58
 */

class FirstUser  extends Seeder
{

    public function run()
    {
        DB::table('users')->delete();
        User::create(array(
            'username' => 'admin',
            'password' => Hash::make('admin'),
            'email'    => 'admin@example.com',
        ));
    }
} 