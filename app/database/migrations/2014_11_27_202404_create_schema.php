<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchema extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        // TODO: Foreign key
        Schema::create('users', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('username', 128);
            $table->string('email', 128);
            $table->string('password', 256);

            $table->string('remember_token', 100)->nullable();
            $table->timestamps();
        });

        Schema::create('tasks', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('title', 128);
            $table->string('description', 128);
            $table->bigInteger('timestamp');
            $table->integer('user');
            $table->integer('status');
            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });

        Schema::create('statuses', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('status', 128);
            $table->string('shortname', 128);
            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });
        
        Schema::create('comments', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('user');
            $table->integer('task');
            $table->longText('comment');
            $table->bigInteger('timestamp');
            $table->dateTime('updated_at');
            $table->dateTime('created_at');
        });

        Schema::create('tasks_users', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('user_id');
            $table->integer('task_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
