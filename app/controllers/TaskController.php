<?php

class TaskController extends \BaseController {

	/**
	 * Display a listing of the tasks.
	 *
	 * @return Response
	 */
	public function index()
	{
        $tasks = Task::select(DB::raw('tasks.*, count(tasks_users.id) as assigned'))
            ->join('tasks_users', 'tasks.id', '=', 'tasks_users.task_id', 'left')
            ->orderBy(DB::raw('(CASE WHEN tasks_users.id IS NULL THEN 0 ELSE 1 END)'))
            ->orderBy('status')
            ->orderBy('timestamp', 'desc')
            ->groupBy('tasks.id')
            ->paginate(10);

        $this->layout->content = View::make('tasks.index')
                ->with('tasks', $tasks);
	}

    /**
     * Shows single task.
     *
     * @param int $id id of the task
     *
     * @return Response
     */
	public function show($id)
	{
        $task           = Task::find($id);
        $comments       = Comment::where('task', '=', $id)->orderBy('timestamp', 'desc')->paginate(10);
        $isAssigned     = $task->users->contains(Auth::id());

        $this->layout->content = View::make('tasks.show')
            ->with('comments', $comments)
            ->with('isAssigned', $isAssigned)
            ->with('task', $task);
	}

    /**
     * Add task.
     *
     * @return Response
     */
	public function create()
	{
        $users = User::lists('username', 'id');
        $statuses = Status::lists('status', 'id');
        $this->layout->content = View::make('tasks.create')
            ->with('users', $users)
            ->with('statuses', $statuses);
	}

    /**
     * Creation process handler
     *
     * @return Response
     */
	public function processCreate()
	{
        $rules = array(
            'title' => 'required',
            'desc'  => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('tasks/create')
                ->withErrors($validator)
                ->withInput(Input::get());
        } else {
            $task               = new Task();
            $task->title        = Input::get('title');
            $task->description  = Input::get('desc');
            $task->user         = Auth::id();
            $task->status       = Input::get('status');
            $task->timestamp    = time();
            $task->save();

            if ( null !== Input::get('user')) {
                $ins = [];
                foreach (Input::get('user') as $user) {
                    $ins[] = ['task_id' => $task->id, 'user_id' => $user];
                }
                // TODO: Remove DB query and override Save() method
                DB::table('tasks_users')->insert($ins);
            }

            return Redirect::to('tasks/' . $task->id)->with('message', 'Task created succesfully')->with('messageType', 'success');
        }
	}

    /**
     * Edit the task.
     *
     * @param int $id id of the task
     *
     * @return Response
     */
	public function edit($id)
	{
        $users      = User::lists('username', 'id');
        $statuses   = Status::lists('status', 'id');
        $task       = Task::find($id);
        $isAssigned = $task->users->contains(Auth::id()) || ! $task->users->count();

        $selectUsers = [];
        foreach ($task->users as $user) {
            $selectUsers[] = $user->id;
        }

        $this->layout->content = View::make('tasks.edit')
            ->with('users', $users)
            ->with('selectUsers', $selectUsers)
            ->with('statuses', $statuses)
            ->with('isAssigned', $isAssigned)
            ->with('task', $task);
	}

    /**
     * Update process handler
     *
     * @return Response
     */
	public function update($id)
	{
        $rules = array(
            'title' => 'required',
            'desc'  => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('tasks/edit/' . $id)
                ->withErrors($validator)
                ->withInput(Input::get());
        } else {
            $task               = Task::find($id);
            $task->title        = Input::get('title');
            $task->description  = Input::get('desc');
            if ( null !== Input::get('status')) {
                 $task->status       = Input::get('status');
            }
            $task->timestamp    = time();

            // TODO: Fixit. Duplicated!!!
            $users = [];
            if ( null !== Input::get('user')) {
                foreach (Input::get('user') as $user) {
                    $users[] = ['task_id' => $task->id, 'user_id' => $user];
                }
            }
            $task->save(['users' => $users]);
            return Redirect::to('tasks/' . $id);
        }
	}

    /**
     * Adding comment
     *
     * @return Response
     */
	public function comment($id)
	{
        $comment            = new Comment();
        $comment->user      = Auth::id();
        $comment->task      = $id;
        $comment->comment   = Input::get('comment');
        $comment->timestamp = time();
        $comment->save();
        return Redirect::to('tasks/' . $id);
	}


}
