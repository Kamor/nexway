<?php

class UserLoginController extends BaseController {

    public $layout = 'layouts.main';

    public function login()
    {
        return View::make('user.login');
    }

    public function processLogin()
    {
        $rules = array(
            'password' => 'required|alphaNum|min:3'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('login')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            $userdata = array(
                'username' 	=> Input::get('username'),
                'password' 	=> Input::get('password'),
            );

            if (Auth::attempt($userdata)) {
                 return Redirect::to('/');
            } else {
                return Redirect::to('login');
            }

        }
    }

    public function logout()
    {
        Auth::logout();
        return Redirect::to('/');
    }

}
