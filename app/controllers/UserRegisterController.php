<?php

class UserRegisterController extends BaseController {

    public $layout = 'layouts.main';

    public function signup() {
        if (Auth::check()) {
            return Redirect::to('/');
        } else {
            return View::make('user.register');
        }
    }

    public function processSignup() {
        $rules = array(
            'username'          => 'required|alphaNum|unique:users|min:3',
            'password'          => 'required|alphaNum|min:3',
            'confirmPassword'   => 'same:password',
            'email'             => 'required|email|unique:users'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('signup')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            $user           = new User();
            $user->username = Input::get('username');
            $user->password = Hash::make( Input::get('username') );
            $user->email    = Input::get('email');
            $user->save();
            return Redirect::to('/')->with('message', 'Account created succesfully')->with('messageType', 'success');
        }
    }
}
