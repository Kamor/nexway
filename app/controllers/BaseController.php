<?php

class BaseController extends Controller {

    /**
     * @property string $layout
     */
    protected $layout = 'layouts.main';

    public function __construct()
    {
        $this->beforeFilter('auth', array('except'  => array('signup','processSignup','login','processLogin')));

        $this->beforeFilter('csrf', array('on' => 'post'));
    }

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
