<?php

class Task extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tasks';

    public function users() {
        return $this->belongsToMany('User', 'tasks_users');
    }

    public function stat() {
        return $this->hasOne('Status', 'id', 'status');
    }

    public function save(array $options = array()) {
        if (isset($options['users'])) {
            DB::table('tasks_users')->where('task_id', '=', $this->id)->delete();
            DB::table('tasks_users')->insert($options['users']);
        }

        return parent::save($options);
    }
}
