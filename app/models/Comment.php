<?php

class Comment extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'comments';

    public function users() {
        return $this->hasOne('User', 'id', 'user');
    }

}
