<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// TODO: Handle Exceptions

Route::get('login', 'UserLoginController@login');
Route::post('login', 'UserLoginController@processLogin');
Route::get('logout', 'UserLoginController@logout');

Route::get('signup', 'UserRegisterController@signup');
Route::post('signup', 'UserRegisterController@processSignup');

Route::get('tasks/create', 'TaskController@create');
Route::post('tasks/create', 'TaskController@processCreate');
Route::get('tasks/edit/{id}', 'TaskController@edit');
Route::post('tasks/edit/{id}', 'TaskController@update');
Route::get('tasks/{id}', 'TaskController@show');
Route::post('tasks/{id}', 'TaskController@comment');
Route::get('tasks', 'TaskController@index');

Route::get('/', 'TaskController@index', array('before' => 'auth'));
